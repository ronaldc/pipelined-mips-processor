module testbench();

  reg         clk;
  reg         reset;

  wire [31:0] writedata, dataadr;
  wire memwrite;
  wire [31:0] pc, instr, readdata;
	
  // instantiate device to be tested
  top dut(clk, reset, writedata, dataadr, memwrite, pc, instr, readdata);
  
  // initialize test
  initial
    begin
      reset = 1; # 100; reset = 0;
    end

  // generate clock to sequence tests
  always
    begin
      clk = 1; # 5; clk = 0; # 5;
    end

  // check that 65 gets written to address 48
  always@(negedge clk)
    begin
		if(memwrite) begin
		  if(dataadr === 0 & writedata === 24) begin
			 $display("Simulation succeeded@@@@@@@@@");
			 $stop;
		  end 
		end
    end
endmodule



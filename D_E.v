`timescale 1ns / 1ps
module D_E(
	input clk,

	input [4:0] RsD,
	input selmulwbD,
	input selHiLoD, RegWriteD, MemtoRegD, MemWriteD, 
	input [2:0] alucontrolD, 
	input alusrcD, regDstD, branchD, WeHiD, WeLoD,
	
	input [31:0] Rf_Rd1D, Rf_Rd2D,
	input [4:0] RtD, RdD, 

	input [31:0] signextD, 
	input [31:0] pcplus4D,

	output reg selHiLoE, RegWriteE, MemtoRegE, MemWriteE, 
	output reg [2:0] alucontrolE, 
	output reg alusrcE, regDstE, branchE, WeHiE, WeLoE,
	
	output reg [31:0] Rf_Rd1E, Rf_Rd2E,
	output reg [4:0] RtE, RdE, 

	output reg [31:0] signextE, 
	output reg [31:0] pcplus4E,

	output reg selmulwbE, 
	output reg [4:0] RsE
    );
	 
	 always @ (posedge clk) 
	 begin
	 	RsE <= RsD;
		selHiLoE <= selHiLoD;
		RegWriteE <= RegWriteD;
		MemtoRegE <= MemtoRegD;
		MemWriteE <= MemWriteD;
		alucontrolE <= alucontrolD;
		alusrcE <= alusrcD;
		regDstE <= regDstD;
		branchE <= branchD;
		WeHiE <= WeHiD;
		WeLoE <= WeLoD;
		Rf_Rd1E <= Rf_Rd1D;
		Rf_Rd2E <= Rf_Rd2D;
		RtE <= RtD;
		RdE <= RdD;
		signextE <= signextD;
		pcplus4E <= pcplus4D;
		selmulwbE <= selmulwbD;
	 end

endmodule

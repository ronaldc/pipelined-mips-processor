`timescale 1ns / 1ps
module AND(
	input branch, zero,
	output pcsrc
    );

	assign pcsrc = branch & zero;

endmodule

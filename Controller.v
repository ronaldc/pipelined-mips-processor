`timescale 1ns / 1ps
// Control Unit
module controller(
	input	[5:0]	op, funct,
	output			memtoreg, memwrite, alusrc, regdst, regwrite, jump, selJR, selRA, selWD, 
						wehi, welo, selhilo, selmulwb,
	output	[2:0]	alucontrol, 
	output branch );

	wire	[1:0]	aluop;

	maindec	md(op, memtoreg, memwrite, branch, alusrc, regdst, regwrite, jump, aluop, selRA, selWD);
	aludec	ad(funct, aluop, alucontrol, selJR, wehi, welo, selhilo, selmulwb);

endmodule
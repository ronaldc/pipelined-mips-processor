`timescale 1ns / 1ps
module F_D(
	input clk,
	input [31:0] instrF, PcPlus4F,
	output reg [31:0] instrD, PcPlus4D);

	always @ (posedge clk)
	begin
		instrD <= instrF;
		PcPlus4D <= PcPlus4F;
	end

endmodule

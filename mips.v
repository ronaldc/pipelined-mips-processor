`timescale 1ns / 1ps
// The MIPS (excluding the instruction and data memories)
module mips(
	input        	clk, reset,
	output	[31:0]	pc,
	input 	[31:0]	instr,
	output			memwriteM,
	output	[31:0]	aluout, writedata,
	input 	[31:0]	readdata,
	input	[ 4:0]	dispSel,
	output	[31:0]	dispDat);

	wire 			memtoreg, alusrc, regdst, regwrite, jump, selJR, selRA, selWD, 
					welo, wehi, selhilo, selmulwb;
	wire	[2:0] 	alucontrol;

	wire branch;
	wire memwriteD;
	wire [31:0] instrD;

	controller c(instrD[31:26], instrD[5:0],
				memtoreg, memwriteD,
				alusrc, regdst, regwrite, jump, selJR, selRA, selWD, wehi, welo, selhilo, selmulwb,
				alucontrol, branch);
	datapath dp(clk, reset, memtoreg,
				alusrc, regdst, regwrite, jump, selJR, selRA, selWD, wehi, welo, selhilo, selmulwb,
				alucontrol, pc, instr, aluout, 
				writedata, readdata, dispSel, dispDat,
				memwriteD, memwriteM, branch, instrD);
endmodule

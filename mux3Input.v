`timescale 1ns / 1ps
// 3-to-1 multiplexer
module mux3Input #(parameter WIDTH = 8) (
	input [WIDTH-1:0] d0, d1, d2,
	input [1:0] sel,
	output reg [WIDTH-1:0] out
    );
	 
	 always @ (*)
	 begin
		case(sel)
			2'b01: out <= d1;
			2'b10: out <= d2;

			default: out <= d0;
		endcase
	end

endmodule

`timescale 1ns / 1ps
module E_M(
	input clk,

	input selmulwbE,
	input selhiloE, regwriteE, memtoregE, memwriteE, 
	input branchE, zeroE, 

	input wehiE, weloE, 

	input [31:0] aluoutE, writedataE, 
	input [4:0] writeregE,

	input [63:0] productE,
	input [31:0] pcbranchE,

	output reg selhiloM, regwriteM, memtoregM, memwriteM, 
	output reg branchM, zeroM, 

	output reg wehiM, weloM, 

	output reg [31:0] aluoutM, writedataM, 
	output reg [4:0] writeregM,

	output reg [63:0] productM,
	output reg [31:0] pcbranchM,

	output reg selmulwbM
    ); 

	always @ (posedge clk)
	begin
		zeroM <= zeroE;
		selhiloM <= selhiloE;
		regwriteM <= regwriteE;
		memtoregM <= memtoregE;
		memwriteM <= memwriteE;
		branchM <= branchE;
		wehiM <= wehiE;
		weloM <= weloE;
		aluoutM <= aluoutE;
		writedataM <= writedataE;
		writeregM <= writeregE;
		productM <= productE;
		pcbranchM <= pcbranchE;
		selmulwbM <= selmulwbE;

	end

endmodule

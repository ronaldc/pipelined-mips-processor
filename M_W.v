`timescale 1ns / 1ps
module M_W(
	input clk,

	input selmulwbM, regwriteM, memtoregM, 
	input [31:0] readdataM, aluoutM, 

	input [4:0] writeregM,
	input [31:0] productM, 

	output reg selmulwbW, regwriteW, memtoregW, 
	output reg [31:0] readdataW, aluoutW, 

	output reg [4:0] writeregW,
	output reg [31:0] productW
    );

	always @ (posedge clk)
	begin 
		selmulwbW <= selmulwbM;
		regwriteW <= regwriteM;
		memtoregW <= memtoregM;
		readdataW <= readdataM;
		aluoutW <= aluoutM;
		writeregW <= writeregM;
		productW <= productM;

	end

endmodule

`timescale 1ns / 1ps
// Data Path (excluding the instruction and data memories)
module datapath(
	input			clk, reset, MemtoRegD, ALUSrcD, RegDstD, RegWriteD, Jump, SelJR, 
				SelRA, SelWD, WeHiD, WeLoD, SelHiLoD, SelMulWbD,
	input	[2:0]	ALUControlD,
	output	[31:0]	pc,
	input	[31:0]	instrF,
	output	[31:0]	ALUOutM, WriteDataM,
	input	[31:0]	ReadDataM,
	input	[ 4:0]	dispSel,
	output	[31:0]	dispDat,
	input 			MemWriteD, 
	output 			MemWriteM, 
	input 			BranchD,
	output 	[31:0] instrD);

	wire [4:0]  WriteRegE, WriteReg2;
	wire [31:0] PCNext, PCNextBr, PCPlus4F, PCBranchE, SignImmD, SignImmSh, SrcAE_1, SrcAE_2, SrcBE, 
					Result1, PCNextBr2, Result3, ProductLo, ProductHi, OutputHiLoM, OutputHiLoW, Result2, ReadDataW;
	wire [63:0] ProductE, ProductM;

	wire [31:0] PcPlus4D;
	
	wire [31:0] SignImmE, Rf_Rd1D, Rf_Rd2D, PCPlus4E, WriteDataE_1, WriteDataE_2, ALUOutE, PCBranchM, ALUOutW;
	wire 		SelHiLoE, RegWriteE, MemtoRegE, MemWriteE, ALUSrcE, RegDstE, BranchE, ZeroE, PCSrc;
	wire [2:0] ALUControlE;
	wire [4:0] RtE, RdE, RsE, WriteRegM, WriteRegW;
	wire	    WeHiE, WeLoE, BranchM, SelHiLoM, RegWriteM, MemtoRegM, ZeroM, WeHiM, WeLoM, SelMulWbE, SelMulWbM,
			SelMulWbW, RegWriteW, MemtoRegW;

	// Hazard Control Unit Signals
	wire [1:0] ForwardAE, ForwardBE;
	

	// ----- Fetch stage -----
	mux2 #(32)  pcbrmux(PCPlus4F, PCBranchM, PCSrc, PCNextBr);								// level-3 mux
	mux2 #(32) 	pcjrmux(PCNextBr, Rf_Rd1D, SelJR, PCNextBr2); //JR instruction				// level-2 mux
	mux2 #(32)  pcmux(PCNextBr2, {PCPlus4F[31:28], instrD[25:0], 2'b00}, Jump, PCNext);		// level-1 mux
		// next PC logic
	flopr #(32) pcreg(clk, reset, PCNext, pc);		// pc-reg
	adder       pcadd1(pc, 32'b100, PCPlus4F);		// pc + 4 
	
	F_D F_D(clk, instrF, PCPlus4F, instrD, PcPlus4D);

	// ----- Decode stage -----
	mux2 #(5)	ramux(WriteRegW, 5'b11111, SelRA, WriteReg2); //JAL instruction  			 // reg file dst sel 
	mux2 #(32)	wdmux(PcPlus4D, Result2, SelWD, Result3); 	// pcplus4 or WB_data -> reg file (mux3)
	signext		se(instrD[15:0], SignImmD);
	
	regfile		rf(clk, RegWriteW, instrD[25:21], instrD[20:16], WriteReg2, Result3, Rf_Rd1D, Rf_Rd2D, dispSel, dispDat);

	D_E D_E(clk, instrD[25:21], SelMulWbD, SelHiLoD, RegWriteD, MemtoRegD, MemWriteD, ALUControlD, ALUSrcD, RegDstD, BranchD,
		WeHiD, WeLoD, Rf_Rd1D, Rf_Rd2D, instrD[20:16], instrD[15:11], SignImmD, PcPlus4D, SelHiLoE, RegWriteE, 
		MemtoRegE, MemWriteE, ALUControlE, ALUSrcE, RegDstE, BranchE, WeHiE, WeLoE, SrcAE_1, WriteDataE_1, RtE, RdE,
		SignImmE, PCPlus4E, SelMulWbE, RsE);

	// ----- Execute stage -----
	mux2 #(5)	wrmux(RtE, RdE, RegDstE, WriteRegE);		// Rt or Rd -> reg file dst
	sl2         immsh(SignImmE, SignImmSh);					// shifter for branch
	adder       pcadd2(PCPlus4E, SignImmSh, PCBranchE);		// adder for branch
	
	mult		mul(SrcAE_2, SrcBE, ProductE); 				//multiplier

	HazardUnit hazardUnit(RsE, RtE, WriteRegM, WriteRegW, RegWriteM, RegWriteW, ForwardAE, ForwardBE);
	mux3Input #(32) srcaE_Mux3(SrcAE_1, Result2, ALUOutM, ForwardAE, SrcAE_2);
	mux3Input #(32) writedataE_Mux3(WriteDataE_1, Result2, ALUOutM, ForwardBE, WriteDataE_2);

		// ALU logic
	mux2 #(32)	srcbmux(WriteDataE_2, SignImmE, ALUSrcE, SrcBE);
	alu			alu(SrcAE_2, SrcBE, ALUControlE, ALUOutE, ZeroE);

	E_M E_M(clk, SelMulWbE, SelHiLoE, RegWriteE, MemtoRegE, MemWriteE, BranchE, ZeroE,	WeHiE, WeLoE, ALUOutE, WriteDataE_2,
		WriteRegE, ProductE, PCBranchE, SelHiLoM, RegWriteM, MemtoRegM, MemWriteM, BranchM, ZeroM, WeHiM, WeLoM, ALUOutM,
		WriteDataM, WriteRegM, ProductM, PCBranchM, SelMulWbM);
	
	// ----- Memory stage -----
	special_reg		mflo(ProductM[31:0], WeLoM, clk, ProductLo); 			// mflo = 1
	special_reg		mfhi(ProductM[63:32], WeHiM, clk, ProductHi); 			// mfhi = 0
	mux2 #(32)		hilomux(ProductLo, ProductHi, SelHiLoM, OutputHiLoM);		// Hi(1) or Lo(0)
		// branch logic
	//AND AND(BranchM, ZeroM, PCSrc);
	assign PCSrc = BranchM & ZeroM;

	M_W M_W(clk, SelMulWbM, RegWriteM, MemtoRegM, ReadDataM, ALUOutM, WriteRegM, OutputHiLoM, SelMulWbW, RegWriteW,
		MemtoRegW, ReadDataW, ALUOutW, WriteRegW, OutputHiLoW);

	// ----- Writeback stage -----
	mux2 #(32)	resmux(ALUOutW, ReadDataW, MemtoRegW, Result1);			// aluout or datamem as WB  (mux1)
	mux2 #(32)  selmulmux(Result1, OutputHiLoW, SelMulWbW, Result2); 	// WB or multWB  (mux2)
	
endmodule
`timescale 1ns / 1ps
module mult(
	input [31:0] a, b,
	output [63:0] result);
	
	assign result = a * b;

endmodule
`timescale 1ns / 1ps
// Instruction Memory
module imem (
	input	[ 9:0]	a,
	output 	[31:0]	dOut );
	
	reg		[31:0]	rom[0:255];
	
	//initialize rom from memfile_s.dat
	initial 
		$readmemh("memfile.dat", rom);
	
	//simple rom
    assign dOut = rom[a];
endmodule
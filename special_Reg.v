`timescale 1ns / 1ps
module special_reg(
	input [31:0] A,
	input we, clk,
	output reg [31:0] B
    );
	
	always @ (posedge clk)
	begin
		if(we) 
			B <= A;
		else
			B <= B;
	end
endmodule
`timescale 1ns / 1ps
// ALU Decoder
module aludec(
	input		[5:0]	funct,
	input		[1:0]	aluop,
	output reg	[2:0]	alucontrol, 
	output selJR, wehi, welo, selhilo, selmulwb);
	
	reg [4:0] cw;
	
	assign {selJR, welo, wehi, selhilo, selmulwb} = cw;

	always @(*)
		case(aluop)
			2'b00: begin alucontrol <= 3'b010; cw <= 5'b00000; end // add
			2'b01: begin alucontrol <= 3'b110; cw <= 5'b00000; end // sub
			default: case(funct)          // RTYPE
				6'b100000: begin alucontrol <= 3'b010; cw <= 5'b00000; end // ADD
				6'b100010: begin alucontrol <= 3'b110; cw <= 5'b00000; end // SUB
				6'b100100: begin alucontrol <= 3'b000; cw <= 5'b00000; end // AND
				6'b100101: begin alucontrol <= 3'b001; cw <= 5'b00000; end // OR
				6'b101010: begin alucontrol <= 3'b111; cw <= 5'b00000; end // SLT
				6'b001000: begin alucontrol <= 3'b000; cw <= 5'b10000; end // JR
				6'b011001: begin alucontrol <= 3'b000; cw <= 5'b01000; end // MULTU
				6'b010010: begin alucontrol <= 3'b000; cw <= 5'b01001; end // MFLO
				6'b010000: begin alucontrol <= 3'b000; cw <= 5'b00111; end // MFHI
				default:   begin alucontrol <= 3'bxxx; cw <= 5'bxxxxx; end // ???
			endcase
		endcase
endmodule
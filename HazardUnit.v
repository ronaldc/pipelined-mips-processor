`timescale 1ns / 1ps
module HazardUnit(

	input [4:0] RsE, RtE,
	input [4:0] WriteRegM, WriteRegW,
	input RegWriteM, RegWriteW,
	output reg [1:0] ForwardAE, ForwardBE,

	input [4:0] RsD, RtD,
	output reg StallF, StallD,
	output reg FlushE,
	input MemtoRegE

    );

	// Forwarding Logic for ForwardAE
	always @ (RsE or WriteRegM or WriteRegW or RegWriteM or  RegWriteW)
	begin
		if ((RsE != 0) && (RsE == WriteRegM) && RegWriteM)
			ForwardAE = 2'b10;
		else if ((RsE != 0) && (RsE == WriteRegW) && RegWriteW)
			ForwardAE = 2'b01;
		else 
			ForwardAE = 2'b00;
	end
	// Forwarding Logic for ForwardBE
	always @ (RtE or  WriteRegM or WriteRegW or RegWriteM or RegWriteW)
	begin
		if ((RtE != 0) && (RtE == WriteRegM) && RegWriteM)
			ForwardBE = 2'b10;
		else if ((RtE != 0) && (RtE == WriteRegW) && RegWriteW)
			ForwardBE = 2'b01;
		else 
			ForwardBE = 2'b00;
	end
	

endmodule

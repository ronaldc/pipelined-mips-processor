`timescale 1ns / 1ps
// Main Decoder
module maindec(
	input	[ 5:0]	op,
	output			memtoreg, memwrite, branch, alusrc, regdst, regwrite, jump,
	output	[ 1:0]	aluop,
	output selRA, selWD); //JR, JAL, 

	reg 	[ 10:0]	controls;

	assign {regwrite, regdst, alusrc, branch, memwrite, memtoreg, jump, aluop, selRA, selWD} = controls;

	always @(*)
		case(op)
			6'b000000: controls <= 11'b110000010_01; //Rtype
			6'b100011: controls <= 11'b101001000_01; //LW
			6'b101011: controls <= 11'b001010000_00; //SW
			6'b000100: controls <= 11'b0x010x001_01; //BEQ
			6'b001000: controls <= 11'b101000000_01; //ADDI
			6'b000010: controls <= 11'b0xxx0x1xx_01; //J
			6'b000011: controls <= 11'b100000100_10; //JAL
			default:   controls <= 11'bxxxxxxxxx_xx; //???
		endcase
endmodule
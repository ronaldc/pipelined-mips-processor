`timescale 1ns / 1ps
module top(input         clk, reset, 
           output [31:0] writedata, dataadr, 
           output        memwrite,
			  output [31:0] pc, instr, readdata);

  //wire [31:0] pc, instr, readdata;
  
  // instantiate processor and memories
  mips mips(clk, reset, pc, instr, memwrite, dataadr, writedata, readdata);
  imem imem(pc[11:2], instr);
  dmem dmem(clk, memwrite, dataadr, writedata, readdata);

endmodule